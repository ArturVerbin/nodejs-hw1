const express = require('express');
const morgan = require('morgan');
const fileRouter = require('./routers/fileRouter');
const fs = require('fs').promises;
const PORT = 8080;
const app = express();

app.use(express.json());
app.use(morgan('tiny'));
(async () => {
    try {
      await fs.mkdir('./files', { recursive: true });
    } catch (error) {
      console.log(error);
    }
  })();
app.use('/api/files', fileRouter);
app.listen(PORT, ()=> console.log('Server works at 8080 port!') );