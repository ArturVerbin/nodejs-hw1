const fs = require('fs');

const checkFileExtensionMiddleware = async (req,res,next) => {
    const fileData = req.body;
    const extensionRegex = /\.(?:log|json|js|txt|yaml|xml)$/;
    const isExtensionCorrect = extensionRegex.test(fileData.filename);
    if(!isExtensionCorrect) {
        res.status(400).json({message: "Enter valid extension"});
    } else {
        next();
    }
}

module.exports = checkFileExtensionMiddleware;