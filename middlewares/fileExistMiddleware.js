const fs = require('fs').promises;

const fileExistMiddleware = async (req,res,next) => {
    const fileName = req.params.filename;
    const files = await fs.readdir('files');
    const searchedFile = files.find(item => item === fileName);

    if(searchedFile) {
        next();
    } else {
        res.status(400).json({message: `No file with '${fileName}' filename found`});
    }
};

module.exports = fileExistMiddleware;