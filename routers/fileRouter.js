const express = require('express');
const router = express.Router();
const fs = require('fs').promises;
const fileExistMiddleware = require('../middlewares/fileExistMiddleware');
const checkFileExtensionMiddleware = require('../middlewares/checkFileExtensionMiddleware');

router.get('/', async (req,res) => {
    try {
        const files = await fs.readdir('files');
        res.status(200).json({
            message: 'Success',
            files
        });
    } catch {
        res.status(500).json({message: 'Server error'});
    }
});

router.get('/:filename', fileExistMiddleware, async (req,res) => {
    try {
        const fileName = req.params.filename;
        const fileContent = await fs.readFile(`files/${fileName}`, 'utf-8');
        const fileExtention = fileName.split('.').pop();
        const fileUploadedDate = await fs.stat(`files/${fileName}`);
        res.status(200).json({
            message: 'Success',
            filename: fileName,
            content: fileContent,
            extension: fileExtention,
            uploadedDate: fileUploadedDate.mtime
        });
    } catch {
        res.status(500).json({message: 'Server error'});
    }
});

router.post('/', checkFileExtensionMiddleware, async (req,res) => {
    try {
        const fileData = req.body;
        if(!fileData.content) {
            res.status(400).json({message: "Please specify 'content' parameter"});
        } else {
            const files = await fs.readdir('files');
            const isExists = files.find(item => item === fileData.filename);
            if(isExists) {
                res.status(400).json({message: "This file already exists"});
            } else {
                fs.writeFile(`files/${fileData.filename}`, fileData.content, 'utf-8');
                res.status(200).json({message: "File created successfully"});
            }
        }
    } catch {
        res.status(500).json({message: 'Server error'});
    }
});

router.delete('/:filename', fileExistMiddleware, async (req,res) => {
    try {
        const fileName = req.params.filename;
        await fs.unlink(`files/${fileName}`);
        res.status(200).json({message: `File '${fileName}' was deleted`});
    } catch {
        res.status(500).json({message: 'Server error'});
    }
});

router.put('/:filename', fileExistMiddleware, async (req,res) => {
    try {
        const fileData= req.body;
        const fileName = req.params.filename;
        const {filename: newFileName, content: newFileContent} = fileData;
        if(newFileName && newFileContent) {
            fs.writeFile(`files/${fileName}`, newFileContent, 'utf-8');
            fs.rename(`files/${fileName}`, `files/${newFileName}`);
            res.status(200).json({message: `File ${fileName} was updated and renamed successfully`});
        } else if(newFileName && !newFileContent) {
            fs.rename(`files/${fileName}`, `files/${newFileName}`);
            res.status(200).json({message: `File ${fileName} was renamed successfully`});
        } else if(!newFileName && newFileContent) {
            fs.writeFile(`files/${fileName}`, newFileContent, 'utf-8');
            res.status(200).json({message: `File ${fileName} was updated successfully`});
        } else {
            res.status(400).json({message: `Please specify 'filename' or 'content' parameter`})
        }
    } catch {
        res.status(500).json({message: 'Server error'});
    }
});

module.exports = router;